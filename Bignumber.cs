﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication13
{

    struct bignumber
    {
        public int[] a;
        public int size;
        public bignumber(string s)
        {
            a = new int[1000];
            size = s.Length;
            int j = 0;
            for (int i = s.Length - 1; i >= 0; i--)
            {
                a[j] = s[i] - '0';
                j++;
            }
        }
        public override string ToString()
        {
            string s = "";
            for (int i = size - 1; i >= 0; i--)
            {
                s = s + a[i].ToString();
            }
            return s;
        }
        public static bignumber operator +(bignumber b, bignumber c)
        {
            int k = 0;
            int length;
            if (b.size > c.size)
            {
                length = b.size;
            }
            else
                length = c.size;
            bignumber d = new bignumber();
            for (int i = 0; i < length; i++)
            {

                b.a[i] = b.a[i] + c.a[i] + k;
                k = b.a[i] / 10;
                b.a[i] %= 10;
            }
            if (k > 0)
            {
                b.size = b.size + 1;
                b.a[d.size - 1] = k;
            }
            return b;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            bignumber c = new bignumber("389448498798784465462");
            Console.WriteLine(c);
            bignumber b = new bignumber("225548554687987878788");
            Console.WriteLine(b);
            bignumber p = new bignumber();
            p = b + c;
            Console.WriteLine(p);
            Console.ReadKey();
        }
    }
}